const webpush = require('web-push');

// VAPID keys should only be generated only once.
const vapidKeys = {
  publicKey:
    'BBZY7Q3KEtZArAAWMLi_qzWHbH4vAoqPpIXnRhmlUaw0PVs1Kt_2fgLhuaVI5i8MWASBKx3d6W6UoH2U3qChw9U',
  privateKey: 'CZtf_JUxmXkCKbzwaKedPPO9BFC99U2rk-GUYDbYAa8'
};

webpush.setVapidDetails(
  'mailto:example@yourdomain.org',
  vapidKeys.publicKey,
  vapidKeys.privateKey
);

// This is the same output of calling JSON.stringify on a PushSubscription
const pushSubscription = {
  endpoint: 'https://fcm.googleapis.com/fcm/send/ec2fdGW7i-k:APA91bGiNmLSZ3GC89fidTcb9AmyJ3MAo1Ff59RuKmj8X75u_2QxpLuwlz2c0QK3Jd39VHBZl-mjlVkd9J5gUel8C3yy0PnZ3e3HoKkoHaexLljABQc2hj1VM8CrBaOv_OjX6_HvfAw0',
  expirationTime: null,
  keys: {
    p256dh: 'BHr-wc1y-ewrSnm5JwwK3F6t-OLnp7B6Y4u-Qo6S_1mZTdDiplp5lAt8KM6PlVZYMxyb2AhCDSrVbFqFFMnXh7k',
    auth: 'OW2fnxh2Bdpb02eiDXI-Dw',
  }
};

webpush.sendNotification(pushSubscription, 'Your Push Payload Text');
