//comment
self.addEventListener("push", function (event) {
  if (event.data) {
    const pushMessage = event.data.text(); // Extract the message from the push event data
    console.log(pushMessage);

    self.clients.matchAll().then(clients => {
      clients.forEach(client => {
        client.postMessage({
          type: "RENDER_CONTENT",
          content: `<h1>${pushMessage}</h1>`,
        });
      });
    });

    event.waitUntil(self.registration.showNotification(pushMessage, {}));
  } else {
    console.log("Push event contains no data.");
  }
});
